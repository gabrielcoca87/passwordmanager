﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PasswordManager.Server.Cryptography;
using PasswordManager.Server.DAL.Repositories;
using PasswordManager.Shared.Dto;
using PasswordManager.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PasswordManager.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardsController : ControllerBase
    {
        private readonly ICardRepository _cardRepository;
        private readonly ICryptographyHelper _cryptographyHelper;
        private readonly IMapper _mapper;

        public CardsController(ICardRepository cardRepository, ICryptographyHelper cryptographyHelper, IMapper mapper)
        {
            _cardRepository = cardRepository;
            _cryptographyHelper = cryptographyHelper;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var cards = await _cardRepository.GetAllAsync();

            if (cards == null)
            {
                return NotFound("Passwords were not found.");
            }

            var cardsDto = _mapper.Map<List<CardDto>>(cards.ToList());

            return Ok(cardsDto);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Card>> GetById(int id)
        {
            if (id == 0)
            {
                return BadRequest("Invalid Id.");
            }

            var card = await _cardRepository.GetAsync(id);

            if (card == null)
            {
                return NotFound("Password was not found.");
            }

            card.Password = await _cryptographyHelper.Decrypt(card.Password);

            return Ok(card);
        }

        [HttpGet("{name}")]
        public async Task<IActionResult> GetByName(string name)
        {
            var cards = await _cardRepository.FindAsync(x => x.Name.Contains(name));

            if (cards == null)
            {
                return NotFound($"Passwords with Name that contains '{ name }' were not found.");
            }

            var cardsDto = _mapper.Map<List<CardDto>>(cards.ToList());

            return Ok(cardsDto);
        }

        [HttpPost]
        public async Task<IActionResult> Create(Card card)
        {
            if (card == null)
            {
                return BadRequest("Invalid data to create the Password.");
            }

            card.Password = await _cryptographyHelper.Encrypt(card.Password);

            var newCard = await _cardRepository.CreateAsync(card);

            return Ok(newCard);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Card card)
        {
            if (card == null)
            {
                return BadRequest("Invalid data to update the Password.");
            }

            if (id == 0)
            {
                return BadRequest("Invalid Id.");
            }

            var cards = await _cardRepository.FindAsync(x => x.Id == id);
            if (cards.Count() == 0)
            {
                return NotFound("Password was not found.");
            }

            card.Password = await _cryptographyHelper.Encrypt(card.Password);

            await _cardRepository.UpdateAsync(card);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest("Invalid Id.");
            }

            await _cardRepository.DeleteAsync(id);

            return Ok();
        }
    }
}
