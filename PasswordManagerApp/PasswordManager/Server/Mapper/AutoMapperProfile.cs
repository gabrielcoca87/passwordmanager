﻿using AutoMapper;
using PasswordManager.Shared.Dto;
using PasswordManager.Shared.Entities;

namespace PasswordManager.Server.Mapper
{
    /// <summary>
    /// Creates object maping.
    /// </summary>
    public class AutoMapperProfile: Profile
    {
        /// <summary>
        /// Maps entities to dtos and viceversa.
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<Card, CardDto>().ReverseMap();
        }
    }
}
