﻿using PasswordManager.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PasswordManager.Server.DAL.Repositories
{
    public interface ICardRepository
    {
        Task<IEnumerable<Card>> GetAllAsync();
        Task<Card> GetAsync(int id);
        Task<Card> CreateAsync(Card card);
        Task DeleteAsync(int id);
        Task UpdateAsync(Card card);
        Task<IEnumerable<Card>> FindAsync(Func<Card, bool> expression);
    }
}
