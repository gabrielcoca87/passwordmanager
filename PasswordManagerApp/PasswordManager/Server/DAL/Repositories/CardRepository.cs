﻿using PasswordManager.Server.DAL.JsonContext.DataPersister;
using PasswordManager.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PasswordManager.Server.DAL.Repositories
{
    /// <summary>
    /// Creates, gets, updates, deletes and finds cards.
    /// </summary>
    public class CardRepository : ICardRepository
    {
        private readonly IDataPersister<Card> _dataPersister;

        public CardRepository(IDataPersister<Card> dataPersister)
        {
            _dataPersister = dataPersister;
        }

        /// <summary>
        /// Gets all the cards.
        /// </summary>
        /// <returns>Cards saved.</returns>
        public async Task<IEnumerable<Card>> GetAllAsync()
        {
            return await _dataPersister.GetAllAsync();
        }

        /// <summary>
        /// Gets a card by id.
        /// </summary>
        /// <param name="id">Id of the card to be returned.</param>
        /// <returns>Card saved.</returns>
        public async Task<Card> GetAsync(int id)
        {
            return await _dataPersister.GetAsync(id);
        }

        /// <summary>
        /// Deletes a card by its id.
        /// </summary>
        /// <param name="id">Id of the card to be deleted.</param>
        /// <returns>Card saved.</returns>
        public async Task DeleteAsync(int id)
        {
            await _dataPersister.DeleteAsync(id);
        }

        /// <summary>
        /// Creates a new card.
        /// </summary>
        /// <param name="card">Card to be created.</param>
        /// <returns>Saved card.</returns>
        public async Task<Card> CreateAsync(Card card)
        {
            return await _dataPersister.CreateAsync(card);
        }

        /// <summary>
        /// Updates a card.
        /// </summary>
        /// <param name="card">Card to be updated.</param>
        /// <returns>Card saved.</returns>
        public async Task UpdateAsync(Card card)
        {
            await _dataPersister.UpdateAsync(card);
        }

        /// <summary>
        /// Finds cards.
        /// </summary>
        /// <param name="expression">Expression for finding cards.</param>
        /// <returns>Cards saved.</returns>
        public async Task<IEnumerable<Card>> FindAsync(Func<Card, bool> expression)
        {
            return await _dataPersister.FindAsync(expression);
        }
    }
}
