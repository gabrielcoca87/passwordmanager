﻿using System.Threading.Tasks;

namespace PasswordManager.Server.DAL.JsonContext.FileHandler
{
    public interface IFileHandler
    {
        string FileName { get; set; }
        Task<string> ReadAsync();
        Task WriteAsync(string content);
    }
}
