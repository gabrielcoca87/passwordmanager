﻿using System.IO;
using System.Threading.Tasks;

namespace PasswordManager.Server.DAL.JsonContext.FileHandler
{
    /// <summary>
    /// Reads and saves data.
    /// </summary>
    public class FileHandler : IFileHandler
    {
        /// <summary>
        /// File name of the file to be read or saved.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Reads a file and return its content.
        /// </summary>
        /// <returns>Content of the file.</returns>
        public async Task<string> ReadAsync()
        {
            string content = await File.ReadAllTextAsync(FileName);
            return content;
        }

        /// <summary>
        /// Saves content into a file.
        /// </summary>
        /// <param name="content">Content that will be saved.</param>
        /// <returns></returns>
        public async Task WriteAsync(string content)
        {
            await File.WriteAllTextAsync(FileName, content);
        }
    }
}
