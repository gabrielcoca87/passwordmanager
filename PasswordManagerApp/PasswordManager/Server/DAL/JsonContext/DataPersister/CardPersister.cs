﻿using PasswordManager.Server.DAL.JsonContext.FileHandler;
using PasswordManager.Server.DAL.Repositories;
using PasswordManager.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.Json;
using System.Threading.Tasks;

namespace PasswordManager.Server.DAL.JsonContext.DataPersister
{
    /// <summary>
    /// Persists cards into the file Cards.json.
    /// </summary>
    public class CardPersister : IDataPersister<Card>
    {
        private readonly IFileHandler _fileHandler;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="fileHandler">Object to read and save data.</param>
        public CardPersister(IFileHandler fileHandler)
        {
            _fileHandler = fileHandler;
            _fileHandler.FileName = $"Cards.json";
        }

        /// <summary>
        /// Gets all the cards.
        /// </summary>
        /// <returns>Cards saved.</returns>
        public async Task<IEnumerable<Card>> GetAllAsync()
        {
            string content = await _fileHandler.ReadAsync();

            var cards = JsonSerializer.Deserialize<IEnumerable<Card>>(content);

            return cards.OrderBy(x => x.Id);
        }

        /// <summary>
        /// Gets a card by id.
        /// </summary>
        /// <param name="id">Id of the card to be returned.</param>
        /// <returns>Card saved.</returns>
        public async Task<Card> GetAsync(int id)
        {
            var cards = await GetAllAsync();

            var card = cards.Where(x => x.Id == id).FirstOrDefault();

            return card;
        }

        /// <summary>
        /// Deletes a card by its id.
        /// </summary>
        /// <param name="id">Id of the card to be deleted.</param>
        /// <returns>Card saved.</returns>
        public async Task DeleteAsync(int id)
        {
            var cards = (await GetAllAsync()).ToList();

            var existingCard = cards.Where(x => x.Id == id).FirstOrDefault();
            
            cards.Remove(existingCard);

            string newContent = JsonSerializer.Serialize(cards);

            await _fileHandler.WriteAsync(newContent);
        }

        /// <summary>
        /// Creates a new card.
        /// </summary>
        /// <param name="card">Card to be created.</param>
        /// <returns>Saved card.</returns>
        public async Task<Card> CreateAsync(Card card)
        {
            var cards = (await GetAllAsync()).ToList();

            int maxId = cards.Count == 0 ? 0 : cards.Max(x => x.Id);
            maxId++;

            card.Id = maxId;
            cards.Add(card);

            string newContent = JsonSerializer.Serialize(cards);

            await _fileHandler.WriteAsync(newContent);

            return card;
        }

        /// <summary>
        /// Updates a card.
        /// </summary>
        /// <param name="card">Card to be updated.</param>
        /// <returns>Card saved.</returns>
        public async Task UpdateAsync(Card card)
        {
            var cards = (await GetAllAsync()).ToList();

            var existingCard = cards.Where(x => x.Id == card.Id).FirstOrDefault();
            
            int index = cards.IndexOf(existingCard);
            cards[index] = card;

            string newContent = JsonSerializer.Serialize(cards);

            await _fileHandler.WriteAsync(newContent);
        }

        /// <summary>
        /// Finds cards.
        /// </summary>
        /// <param name="expression">Expression for finding cards.</param>
        /// <returns>Cards saved.</returns>
        public async Task<IEnumerable<Card>> FindAsync(Func<Card, bool> expression)
        {
            string content = await _fileHandler.ReadAsync();

            var cards = (await GetAllAsync()).ToList();

            return cards.Where(expression);
        }
    }
}
