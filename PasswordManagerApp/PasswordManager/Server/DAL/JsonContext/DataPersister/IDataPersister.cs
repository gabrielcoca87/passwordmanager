﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PasswordManager.Server.DAL.JsonContext.DataPersister
{
    public interface IDataPersister<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetAsync(int id);
        Task<T> CreateAsync(T record);
        Task DeleteAsync(int id);
        Task UpdateAsync(T record);
        Task<IEnumerable<T>> FindAsync(Func<T, bool> expression);
    }
}
