﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PasswordManager.Server.Cryptography
{
    public interface ICryptographyHelper
    {
        Task<string> Encrypt(string plainText);
        Task<string> Decrypt(string encriptedText);
    }
}
