﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager.Server.Cryptography
{
    /// <summary>
    /// Encrypts and decrypts texts using symmetric algorithm AES (Advanced Encryption Standard).
    /// </summary>
    public class CryptographyHelper : ICryptographyHelper
    {
        private readonly string _key;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="key">Key used to encrypt and decrypt texts.</param>
        public CryptographyHelper(string key)
        {
            _key = key;
        }

        /// <summary>
        /// Encrypts a plain text.
        /// </summary>
        /// <param name="plainText">Text to be encrypted.</param>
        /// <returns>Encrypted text.</returns>
        public async Task<string> Encrypt(string plainText)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(_key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            await streamWriter.WriteAsync(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }

        /// <summary>
        /// Decrypts an encrypted text to its original value.
        /// </summary>
        /// <param name="encryptedText">Text to be decrypted.</param>
        /// <returns>Decrypted text.</returns>
        public async Task<string> Decrypt(string encryptedText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(encryptedText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(_key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return await streamReader.ReadToEndAsync();
                        }
                    }
                }
            }
        }
    }
}
