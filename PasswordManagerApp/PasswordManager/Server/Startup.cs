using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PasswordManager.Server.Cryptography;
using PasswordManager.Server.DAL.JsonContext;
using PasswordManager.Server.DAL.JsonContext.DataPersister;
using PasswordManager.Server.DAL.JsonContext.FileHandler;
using PasswordManager.Server.DAL.Repositories;
using PasswordManager.Shared.Entities;
using System.IO;
using System.Net;
using System.Text.Json;

namespace PasswordManager.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddAutoMapper(typeof(Startup));

            #region Dependency injection of my own interfaces and classes
            services.AddScoped<IFileHandler, FileHandler>();
            services.AddScoped(typeof(IDataPersister<Card>), typeof(CardPersister));
            services.AddScoped<ICardRepository, CardRepository>();
            services.AddScoped<ICryptographyHelper>(x => new CryptographyHelper(Configuration.GetValue<string>("Key")));
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();

                //Creates an exception handler to return an internal server error on unhandled exceptions.
                app.UseExceptionHandler(appError =>
                {
                    appError.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "application/json";

                        var exception = context.Features.Get<IExceptionHandlerFeature>();
                        if (exception != null)
                        {
                            var result = JsonSerializer.Serialize(new { Message = exception.Error.Message });
                            await context.Response.WriteAsync(result);
                        }
                    });
                });
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}
