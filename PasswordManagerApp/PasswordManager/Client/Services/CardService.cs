﻿using PasswordManager.Shared.Entities;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using System.Linq;
using PasswordManager.Shared.Dto;
using System;

namespace PasswordManager.Client.Services
{
    /// <summary>
    /// Provides communication with Cards Web Api.
    /// </summary>
    public class CardService : ICardService
    {
        private readonly HttpClient _httpClient;

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="httpClient">Client to stablish communication with Web Api.</param>
        public CardService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Gets all the cards.
        /// </summary>
        /// <returns>Cards.</returns>
        public async Task<List<CardDto>> GetAllCards()
        {
            var response = await _httpClient.GetAsync("api/cards");
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return await response.Content.ReadFromJsonAsync<List<CardDto>>();
        }

        /// <summary>
        /// Gets all the cards.
        /// </summary>
        /// <param name="id">Id of the card to be returned.</param>
        /// <returns>Card</returns>
        public async Task<Card> GetCard(int id)
        {
            var card = await _httpClient.GetFromJsonAsync<Card>($"api/cards/{ id }");
            return card;
        }

        /// <summary>
        /// Updates a card.
        /// </summary>
        /// <param name="id">Id of the card to be updated.</param>
        /// <param name="card">Card to be updated.</param>
        /// <returns>Success</returns>
        public async Task<bool> UpdateCard(int id, Card card)
        {
            var response = await _httpClient.PutAsJsonAsync($"api/cards/{ id }", card);
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a new card.
        /// </summary>
        /// <param name="card">Card to be created.</param>
        /// <returns>Card</returns>
        public async Task<Card> CreateCard(Card card)
        {
            var response = await _httpClient.PostAsJsonAsync("api/cards", card);
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            return await response.Content.ReadFromJsonAsync<Card>();
        }

        /// <summary>
        /// Deletes a card by its id.
        /// </summary>
        /// <param name="id">Id of the card to be deleted.</param>
        /// <returns>Success</returns>
        public async Task<bool> DeleteCard(int id)
        {
            var response = await _httpClient.DeleteAsync($"api/cards/{ id }");
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Searches for cards.
        /// </summary>
        /// <param name="name">Name of the card to be searched.</param>
        /// <returns>Cards.</returns>
        public async Task<List<CardDto>> SearchByName(string name)
        {
            var cards = await _httpClient.GetFromJsonAsync<List<CardDto>>($"api/cards/{ name }");
            return cards;
        }
    }
}
