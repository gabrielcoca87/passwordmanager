﻿using PasswordManager.Shared.Dto;
using PasswordManager.Shared.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PasswordManager.Client.Services
{
    public interface ICardService
    {
        Task<List<CardDto>> GetAllCards();
        Task<Card> GetCard(int id);
        Task<bool> UpdateCard(int id, Card card);
        Task<Card> CreateCard(Card card);
        Task<bool> DeleteCard(int id);
        Task<List<CardDto>> SearchByName(string name);
    }
}
