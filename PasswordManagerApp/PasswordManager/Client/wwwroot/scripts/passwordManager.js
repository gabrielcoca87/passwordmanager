﻿function showPassword(show) {
    var cardPassword = document.getElementById("cardPassword");

    if (show) {
        cardPassword.setAttribute("type", "password");
    } else {
        cardPassword.setAttribute("type", "text");
    }
}

function deletePassword(text) {
    return confirm(text);
}

window.clipboardCopy = {
    copyText: function (text) {
        navigator.clipboard.writeText(text)
            .then(function () {
                alert("Copied to clipboard!");
            })
            .catch(function (error) {
                alert(error);
            });
    }
};