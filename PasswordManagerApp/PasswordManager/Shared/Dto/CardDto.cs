﻿namespace PasswordManager.Shared.Dto
{
    /// <summary>
    /// Holds basic information of a Card.
    /// </summary>
    public class CardDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Username { get; set; }
    }
}
