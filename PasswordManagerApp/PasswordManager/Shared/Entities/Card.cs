﻿using System.ComponentModel.DataAnnotations;

namespace PasswordManager.Shared.Entities
{
    /// <summary>
    /// Holds necessary information of a Card.
    /// </summary>
    public class Card
    {
        public int Id { get; set; }
        [Required]
        public string Url { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
