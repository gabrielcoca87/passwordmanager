using Moq;
using PasswordManager.Server.DAL.JsonContext.DataPersister;
using PasswordManager.Server.DAL.Repositories;
using PasswordManager.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PasswordManager.Tests
{
    public class CardRepositoryTest
    {
        [Fact]
        public async Task GetAllAsync_Empty_ReturnsCards()
        {
            //Arrange
            var cards = new List<Card>
            {
                new Card
                {
                    Id = 1,
                    Name = "Amazon",
                    Url = "https://wwww.amazon.com/login",
                    Username = "gcoca",
                    Password = "encryptedpassword1"
                },
                new Card
                {
                    Id = 2,
                    Name = "Facebook",
                    Url = "https://wwww.facebook.com/login",
                    Username = "gabrielcoca",
                    Password = "encryptedpassword2"
                }
            };

            var mockDataPersister = new Mock<IDataPersister<Card>>();
            mockDataPersister.Setup(s => s.GetAllAsync()).ReturnsAsync(cards);

            var cardRepository = new CardRepository(mockDataPersister.Object);

            //Act
            var result = await cardRepository.GetAllAsync();

            //Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
            Assert.Equal("Amazon", result.FirstOrDefault().Name);
            Assert.Equal("Facebook", result.LastOrDefault().Name);

            mockDataPersister.Verify(x => x.GetAllAsync());
        }

        [Fact]
        public async Task GetAsync_CardId_ReturnsCard()
        {
            //Arrange
            var card = new Card
            {
                Id = 1,
                Name = "Amazon",
                Url = "https://wwww.amazon.com/login",
                Username = "gcoca",
                Password = "encryptedpassword"
            };

            var mockDataPersister = new Mock<IDataPersister<Card>>();
            mockDataPersister.Setup(s => s.GetAsync(It.IsAny<int>())).ReturnsAsync(card);

            var cardRepository = new CardRepository(mockDataPersister.Object);

            //Act
            var result = await cardRepository.GetAsync(1);

            //Assert
            Assert.NotNull(result);
            Assert.Equal("Amazon", result.Name);

            mockDataPersister.Verify(x => x.GetAsync(It.IsAny<int>()));
        }

        [Fact]
        public async Task DeleteAsync_CardId_ReturnsEmpty()
        {
            //Arrange
            var mockDataPersister = new Mock<IDataPersister<Card>>();
            mockDataPersister.Setup(s => s.DeleteAsync(It.IsAny<int>()));

            var cardRepository = new CardRepository(mockDataPersister.Object);

            //Act
            await cardRepository.DeleteAsync(1);

            //Assert
            mockDataPersister.Verify(x => x.DeleteAsync(It.IsAny<int>()));
        }

        [Fact]
        public async Task CreateAsync_Card_ReturnsCard()
        {
            //Arrange
            var card = new Card
            {
                Name = "Amazon",
                Url = "https://wwww.amazon.com/login",
                Username = "gcoca",
                Password = "encryptedpassword"
            };

            var mockDataPersister = new Mock<IDataPersister<Card>>();
            mockDataPersister.Setup(s => s.CreateAsync(It.IsAny<Card>())).ReturnsAsync(card);

            var cardRepository = new CardRepository(mockDataPersister.Object);

            //Act
            var newCard = await cardRepository.CreateAsync(card);

            //Assert
            Assert.NotNull(newCard);
            Assert.Equal(card.Name, newCard.Name);

            mockDataPersister.Verify(x => x.CreateAsync(It.IsAny<Card>()));
        }

        [Fact]
        public async Task UpdateAsync_Card_ReturnsEmpty()
        {
            //Arrange
            var card = new Card
            {
                Name = "Amazon",
                Url = "https://wwww.amazon.com/login",
                Username = "gcoca",
                Password = "encryptedpassword"
            };

            var mockDataPersister = new Mock<IDataPersister<Card>>();
            mockDataPersister.Setup(s => s.UpdateAsync(It.IsAny<Card>()));

            var cardRepository = new CardRepository(mockDataPersister.Object);

            //Act
            await cardRepository.UpdateAsync(card);

            //Assert
            mockDataPersister.Verify(x => x.UpdateAsync(It.IsAny<Card>()));
        }

        [Fact]
        public async Task FindAsync_CardName_ReturnsCards()
        {
            //Arrange
            var cards = new List<Card>
            {
                new Card
                {
                    Id = 1,
                    Name = "Amazon",
                    Url = "https://wwww.amazon.com/login",
                    Username = "gcoca",
                    Password = "encryptedpassword1"
                }
            };

            var mockDataPersister = new Mock<IDataPersister<Card>>();
            mockDataPersister.Setup(s => s.FindAsync(It.IsAny<Func<Card, bool>>())).ReturnsAsync(cards);

            var cardRepository = new CardRepository(mockDataPersister.Object);

            //Act
            var result = await cardRepository.FindAsync(x => x.Name == "Amazon");

            //Assert
            Assert.NotNull(result);
            Assert.Single(result);
            Assert.Equal("Amazon", result.FirstOrDefault().Name);

            mockDataPersister.Verify(x => x.FindAsync(It.IsAny<Func<Card, bool>>()));
        }
    }
}
